# Calculator

A simple calculator made in Java language with Netbeans IDE.

![Calculator](./assets/calculator.png)

## Requirements

[JLayer 1.0.1](http://www.javazoom.net/javalayer/javalayer.html)

## More

This calculator is a work in progress, feel free to improve it.

## License

This simple calculator is released under the MIT license. See [LICENSE](./LICENSE) for details.
