package calculadora;

import SobreDesenvolvedores.JFSobre;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.io.*;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

public class JFCalc extends javax.swing.JFrame {

    public char operacao;
    public String temp, numeroAntes, copia;
    public double num1;
    public double num2;
    public double resultado;
    int vetN[] = {128, 64, 32, 16, 8, 4, 2, 1};
    int vetB[] = new int[8];
    int contMusicas=0;
    private Player player;

    public JFCalc() {
        setResizable(false);
        initComponents();
        JFCalc.this.setSize(296, 450);
        setLocation(170, 180);
        setTitle("Calculadora");
        txtNumero2.setEnabled(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jFileChooser1 = new javax.swing.JFileChooser();
        buttonGroup1 = new javax.swing.ButtonGroup();
        pnCalculadora = new javax.swing.JPanel();
        JTVisor = new javax.swing.JTextField();
        jBBackspace = new javax.swing.JButton();
        jB7 = new javax.swing.JButton();
        jB4 = new javax.swing.JButton();
        jB1 = new javax.swing.JButton();
        jB0 = new javax.swing.JButton();
        jBPonto = new javax.swing.JButton();
        jB2 = new javax.swing.JButton();
        jB5 = new javax.swing.JButton();
        jB8 = new javax.swing.JButton();
        jBCE = new javax.swing.JButton();
        jBClear = new javax.swing.JButton();
        jB9 = new javax.swing.JButton();
        jB6 = new javax.swing.JButton();
        jB3 = new javax.swing.JButton();
        jBAdicao = new javax.swing.JButton();
        jBMenosMais = new javax.swing.JButton();
        jBDivisao = new javax.swing.JButton();
        jBMultiplicacao = new javax.swing.JButton();
        JBSubtracao = new javax.swing.JButton();
        jBIgual = new javax.swing.JButton();
        pnFatorial = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txtNumeroFatorial = new javax.swing.JTextField();
        pnEquacao1Grau = new javax.swing.JPanel();
        txtValor2 = new javax.swing.JTextField();
        lblXEquacao = new javax.swing.JLabel();
        lblIgualEquacao = new javax.swing.JLabel();
        txtResultado = new javax.swing.JTextField();
        txtValor1 = new javax.swing.JTextField();
        pnRadiciacao = new javax.swing.JPanel();
        txtNumeroRadiciacao = new javax.swing.JTextField();
        lblNumeroRadiciacao = new javax.swing.JLabel();
        pnPotenciacao = new javax.swing.JPanel();
        txtBase = new javax.swing.JTextField();
        txtExpoente = new javax.swing.JTextField();
        lblBase = new javax.swing.JLabel();
        lblExpoente = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        pnBinario = new javax.swing.JPanel();
        txtNumero1 = new javax.swing.JTextField();
        lblNumero1 = new javax.swing.JLabel();
        radioBinarioNumero = new javax.swing.JRadioButton();
        radioNumeroBinario = new javax.swing.JRadioButton();
        lblNumero2 = new javax.swing.JLabel();
        txtNumero2 = new javax.swing.JTextField();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        itemMenuSair = new javax.swing.JMenuItem();
        itemMenuFuncionalidades = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        itemMenuAbrir = new javax.swing.JMenuItem();
        itemMenuFechar = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(204, 204, 204));

        pnCalculadora.setBackground(new java.awt.Color(102, 102, 102));

        JTVisor.setFont(new java.awt.Font("Vani", 1, 36)); // NOI18N
        JTVisor.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        JTVisor.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        JTVisor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JTVisorActionPerformed(evt);
            }
        });

        jBBackspace.setText("Bsp");
        jBBackspace.setToolTipText("Apaga o último caractere digitado...");
        jBBackspace.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jBBackspace.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBackspaceActionPerformed(evt);
            }
        });

        jB7.setText("7");
        jB7.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jB7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB7ActionPerformed(evt);
            }
        });

        jB4.setText("4");
        jB4.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jB4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB4ActionPerformed(evt);
            }
        });

        jB1.setText("1");
        jB1.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jB1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB1ActionPerformed(evt);
            }
        });

        jB0.setText("0");
        jB0.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jB0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB0ActionPerformed(evt);
            }
        });

        jBPonto.setText(".");
        jBPonto.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jBPonto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBPontoActionPerformed(evt);
            }
        });

        jB2.setText("2");
        jB2.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jB2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB2ActionPerformed(evt);
            }
        });

        jB5.setText("5");
        jB5.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jB5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB5ActionPerformed(evt);
            }
        });

        jB8.setText("8");
        jB8.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jB8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB8ActionPerformed(evt);
            }
        });

        jBCE.setText("CE");
        jBCE.setToolTipText("Zera todos os Valores...");
        jBCE.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jBCE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCEActionPerformed(evt);
            }
        });

        jBClear.setText("Clr");
        jBClear.setToolTipText("Apaga tudo...");
        jBClear.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jBClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBClearActionPerformed(evt);
            }
        });

        jB9.setText("9");
        jB9.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jB9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB9ActionPerformed(evt);
            }
        });

        jB6.setText("6");
        jB6.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jB6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB6ActionPerformed(evt);
            }
        });

        jB3.setText("3");
        jB3.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jB3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB3ActionPerformed(evt);
            }
        });

        jBAdicao.setText("+");
        jBAdicao.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jBAdicao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAdicaoActionPerformed(evt);
            }
        });

        jBMenosMais.setText("-+");
        jBMenosMais.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jBMenosMais.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBMenosMaisActionPerformed(evt);
            }
        });

        jBDivisao.setText("/");
        jBDivisao.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jBDivisao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBDivisaoActionPerformed(evt);
            }
        });

        jBMultiplicacao.setText("*");
        jBMultiplicacao.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jBMultiplicacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBMultiplicacaoActionPerformed(evt);
            }
        });

        JBSubtracao.setText("-");
        JBSubtracao.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        JBSubtracao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBSubtracaoActionPerformed(evt);
            }
        });

        jBIgual.setText("=");
        jBIgual.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jBIgual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBIgualActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnCalculadoraLayout = new javax.swing.GroupLayout(pnCalculadora);
        pnCalculadora.setLayout(pnCalculadoraLayout);
        pnCalculadoraLayout.setHorizontalGroup(
            pnCalculadoraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnCalculadoraLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(pnCalculadoraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnCalculadoraLayout.createSequentialGroup()
                        .addComponent(jBBackspace, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(jBCE, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(jBClear, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(jBMenosMais, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnCalculadoraLayout.createSequentialGroup()
                        .addComponent(jB7, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(jB8, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(jB9, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(jBDivisao, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnCalculadoraLayout.createSequentialGroup()
                        .addComponent(jB4, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(jB5, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(jB6, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(jBMultiplicacao, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnCalculadoraLayout.createSequentialGroup()
                        .addComponent(jB1, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(jB2, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(jB3, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(JBSubtracao, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnCalculadoraLayout.createSequentialGroup()
                        .addComponent(jB0, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(jBPonto, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(jBAdicao, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(jBIgual, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 12, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnCalculadoraLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(JTVisor)
                .addContainerGap())
        );
        pnCalculadoraLayout.setVerticalGroup(
            pnCalculadoraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnCalculadoraLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(JTVisor, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addGroup(pnCalculadoraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jBBackspace, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBCE, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBClear, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBMenosMais, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(pnCalculadoraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jB7, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jB8, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jB9, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBDivisao, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(pnCalculadoraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jB4, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jB5, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jB6, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBMultiplicacao, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(pnCalculadoraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jB1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jB2, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jB3, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JBSubtracao, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(pnCalculadoraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jB0, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBPonto, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBAdicao, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBIgual, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnFatorial.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Fatorial", 2, 0));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Número:");

        javax.swing.GroupLayout pnFatorialLayout = new javax.swing.GroupLayout(pnFatorial);
        pnFatorial.setLayout(pnFatorialLayout);
        pnFatorialLayout.setHorizontalGroup(
            pnFatorialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnFatorialLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNumeroFatorial, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(76, Short.MAX_VALUE))
        );
        pnFatorialLayout.setVerticalGroup(
            pnFatorialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnFatorialLayout.createSequentialGroup()
                .addGroup(pnFatorialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtNumeroFatorial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        pnEquacao1Grau.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Equação 1º grau", 2, 0));

        lblXEquacao.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblXEquacao.setText("x + ");

        lblIgualEquacao.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblIgualEquacao.setText("=");

        javax.swing.GroupLayout pnEquacao1GrauLayout = new javax.swing.GroupLayout(pnEquacao1Grau);
        pnEquacao1Grau.setLayout(pnEquacao1GrauLayout);
        pnEquacao1GrauLayout.setHorizontalGroup(
            pnEquacao1GrauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnEquacao1GrauLayout.createSequentialGroup()
                .addComponent(txtValor1, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblXEquacao)
                .addGap(8, 8, 8)
                .addComponent(txtValor2, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblIgualEquacao, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtResultado, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        pnEquacao1GrauLayout.setVerticalGroup(
            pnEquacao1GrauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnEquacao1GrauLayout.createSequentialGroup()
                .addGroup(pnEquacao1GrauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblXEquacao)
                    .addComponent(lblIgualEquacao)
                    .addComponent(txtResultado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtValor1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtValor2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        pnRadiciacao.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Radiciação", 2, 0));

        txtNumeroRadiciacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNumeroRadiciacaoActionPerformed(evt);
            }
        });

        lblNumeroRadiciacao.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblNumeroRadiciacao.setText("Número:");

        javax.swing.GroupLayout pnRadiciacaoLayout = new javax.swing.GroupLayout(pnRadiciacao);
        pnRadiciacao.setLayout(pnRadiciacaoLayout);
        pnRadiciacaoLayout.setHorizontalGroup(
            pnRadiciacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnRadiciacaoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblNumeroRadiciacao)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtNumeroRadiciacao, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(107, Short.MAX_VALUE))
        );
        pnRadiciacaoLayout.setVerticalGroup(
            pnRadiciacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnRadiciacaoLayout.createSequentialGroup()
                .addGroup(pnRadiciacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNumeroRadiciacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNumeroRadiciacao))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        pnPotenciacao.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Potenciação", 2, 0));

        lblBase.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblBase.setText("Base:");

        lblExpoente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblExpoente.setText("Expoente");

        javax.swing.GroupLayout pnPotenciacaoLayout = new javax.swing.GroupLayout(pnPotenciacao);
        pnPotenciacao.setLayout(pnPotenciacaoLayout);
        pnPotenciacaoLayout.setHorizontalGroup(
            pnPotenciacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnPotenciacaoLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblBase)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtBase, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblExpoente)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtExpoente, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27))
        );
        pnPotenciacaoLayout.setVerticalGroup(
            pnPotenciacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnPotenciacaoLayout.createSequentialGroup()
                .addGroup(pnPotenciacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblBase)
                    .addComponent(lblExpoente)
                    .addComponent(txtBase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtExpoente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        jButton1.setText("Calcular");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        pnBinario.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Conversão de decimal em binário", 2, 0));

        txtNumero1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtNumero1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNumero1KeyReleased(evt);
            }
        });

        lblNumero1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblNumero1.setText("Número:");

        buttonGroup1.add(radioBinarioNumero);
        radioBinarioNumero.setText("Binário → Número");
        radioBinarioNumero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioBinarioNumeroActionPerformed(evt);
            }
        });

        buttonGroup1.add(radioNumeroBinario);
        radioNumeroBinario.setSelected(true);
        radioNumeroBinario.setText("Número → Binário");
        radioNumeroBinario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioNumeroBinarioActionPerformed(evt);
            }
        });

        lblNumero2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblNumero2.setText("Binário:");

        txtNumero2.setEditable(false);
        txtNumero2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout pnBinarioLayout = new javax.swing.GroupLayout(pnBinario);
        pnBinario.setLayout(pnBinarioLayout);
        pnBinarioLayout.setHorizontalGroup(
            pnBinarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnBinarioLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnBinarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnBinarioLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(pnBinarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnBinarioLayout.createSequentialGroup()
                                .addComponent(lblNumero1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(txtNumero1, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnBinarioLayout.createSequentialGroup()
                                .addComponent(radioNumeroBinario, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(radioBinarioNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(pnBinarioLayout.createSequentialGroup()
                        .addComponent(lblNumero2, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtNumero2, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(107, Short.MAX_VALUE))
        );
        pnBinarioLayout.setVerticalGroup(
            pnBinarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnBinarioLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnBinarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblNumero1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNumero1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(pnBinarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioNumeroBinario)
                    .addComponent(radioBinarioNumero))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnBinarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNumero2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNumero2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(13, Short.MAX_VALUE))
        );

        jMenu1.setText("Calculadora");

        itemMenuSair.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.ALT_MASK));
        itemMenuSair.setText("Sair");
        itemMenuSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemMenuSairActionPerformed(evt);
            }
        });
        jMenu1.add(itemMenuSair);

        itemMenuFuncionalidades.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        itemMenuFuncionalidades.setText("Mais Funcionalidades");
        itemMenuFuncionalidades.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemMenuFuncionalidadesActionPerformed(evt);
            }
        });
        jMenu1.add(itemMenuFuncionalidades);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Editar");

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setText("Copiar");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem2);

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem3.setText("Colar");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Sobre");
        jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu3MouseClicked(evt);
            }
        });

        jMenuItem1.setText("Desenvolvedores");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem1);

        jMenuBar1.add(jMenu3);

        jMenu5.setText("Música");
        jMenu5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu5MouseClicked(evt);
            }
        });

        itemMenuAbrir.setText("Abrir");
        itemMenuAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemMenuAbrirActionPerformed(evt);
            }
        });
        jMenu5.add(itemMenuAbrir);

        itemMenuFechar.setText("Fechar");
        itemMenuFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemMenuFecharActionPerformed(evt);
            }
        });
        jMenu5.add(itemMenuFechar);

        jMenuBar1.add(jMenu5);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pnCalculadora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(pnEquacao1Grau, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(pnFatorial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(pnPotenciacao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(pnRadiciacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(pnBinario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(203, 203, 203)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(pnPotenciacao, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pnEquacao1Grau, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(pnFatorial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pnRadiciacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnBinario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(29, 29, 29)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29))
            .addComponent(pnCalculadora, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private String getFormat(File file) {
        char charFormato[] = String.valueOf(file).toCharArray();
        return String.valueOf(charFormato[charFormato.length - 4]) + String.valueOf(charFormato[charFormato.length - 3]) + String.valueOf(charFormato[charFormato.length - 2]) + String.valueOf(charFormato[charFormato.length - 1]);
    }

public void tocar(File file) {
        if (getFormat(file).equals(".mp3")) {
            try {

                if (contMusicas == 0) {
                    FileInputStream fis = new FileInputStream(file);
                    BufferedInputStream bis = new BufferedInputStream(fis);
                    this.player = new Player(bis);
                    this.player.play();
                    contMusicas++;
                } else {
                    JOptionPane.showMessageDialog(null, "Encerre a Música atual para poder abrir outra", "Erro", JOptionPane.ERROR_MESSAGE);
                }
            } catch (HeadlessException | FileNotFoundException | JavaLayerException e) {
                JOptionPane.showMessageDialog(null, "Formato Incompatível", "Erro", JOptionPane.ERROR_MESSAGE);

            }
        } else {
            try {
                if (contMusicas == 0) {
                    InputStream in = new FileInputStream(file);
                    audio = new AudioStream(in);
                    AudioPlayer.player.start(audio);
                    contMusicas++;

                } else {
                    JOptionPane.showMessageDialog(null, "Encerre a Música atual para poder abrir outra", "Erro", JOptionPane.ERROR_MESSAGE);
                }
            } catch (IOException | HeadlessException e) {
                JOptionPane.showMessageDialog(null, "Formato Incompatível", "Erro", JOptionPane.ERROR_MESSAGE);

            }
        }
    }
    
    public void operacaoDesejada(char a) {
        try {
            operacao = a;
            num1 = Double.parseDouble(JTVisor.getText());
            JTVisor.setText(null);
        } catch (NumberFormatException e) {
            e.toString();
            JOptionPane.showMessageDialog(null, "Digite algum número para que se possa fazer a operação desejada!!!", "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }
    private void jB6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB6ActionPerformed
        Toolkit.getDefaultToolkit().beep();
        numeroDigitado("6");
    }//GEN-LAST:event_jB6ActionPerformed

    private void jB0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB0ActionPerformed
        Toolkit.getDefaultToolkit().beep();
        numeroDigitado("0");
    }//GEN-LAST:event_jB0ActionPerformed

    private void jB2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB2ActionPerformed
        Toolkit.getDefaultToolkit().beep();
        numeroDigitado("2");
    }

    public void numeroDigitado(String a) {
        try {
            temp = JTVisor.getText();
            numeroAntes = temp;
            temp = temp + a;
            JTVisor.setText(temp);
        } catch (Exception e) {
            e.getMessage();
            JOptionPane.showMessageDialog(null, "Digite apenas números!!!", "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jB2ActionPerformed

    private void jB3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB3ActionPerformed
        Toolkit.getDefaultToolkit().beep();
        numeroDigitado("3");
    }//GEN-LAST:event_jB3ActionPerformed

    private void jB4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB4ActionPerformed
        Toolkit.getDefaultToolkit().beep();
        numeroDigitado("4");
    }//GEN-LAST:event_jB4ActionPerformed

    private void jB5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB5ActionPerformed
        Toolkit.getDefaultToolkit().beep();
        numeroDigitado("5");
    }//GEN-LAST:event_jB5ActionPerformed

    private void jB1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB1ActionPerformed
        Toolkit.getDefaultToolkit().beep();
        numeroDigitado("1");
    }//GEN-LAST:event_jB1ActionPerformed

    private void jB7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB7ActionPerformed
        Toolkit.getDefaultToolkit().beep();
        numeroDigitado("7");
    }//GEN-LAST:event_jB7ActionPerformed

    private void jB8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB8ActionPerformed
        Toolkit.getDefaultToolkit().beep();
        numeroDigitado("8");
    }//GEN-LAST:event_jB8ActionPerformed

    private void jB9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB9ActionPerformed
        Toolkit.getDefaultToolkit().beep();
        numeroDigitado("9");
    }//GEN-LAST:event_jB9ActionPerformed

    private void jBPontoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBPontoActionPerformed
        Toolkit.getDefaultToolkit().beep();
        numeroDigitado(".");
    }//GEN-LAST:event_jBPontoActionPerformed

    private void jBAdicaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAdicaoActionPerformed
        Toolkit.getDefaultToolkit().beep();
        operacaoDesejada('+');

    }//GEN-LAST:event_jBAdicaoActionPerformed

    private void JBSubtracaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBSubtracaoActionPerformed
        Toolkit.getDefaultToolkit().beep();
        operacaoDesejada('-');
    }//GEN-LAST:event_JBSubtracaoActionPerformed

    private void jBMultiplicacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBMultiplicacaoActionPerformed
        Toolkit.getDefaultToolkit().beep();
        operacaoDesejada('*');
    }//GEN-LAST:event_jBMultiplicacaoActionPerformed

    private void jBDivisaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBDivisaoActionPerformed
        Toolkit.getDefaultToolkit().beep();
        operacaoDesejada('/');
    }//GEN-LAST:event_jBDivisaoActionPerformed

    private void jBClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBClearActionPerformed
        Toolkit.getDefaultToolkit().beep();
        JTVisor.setText(null);
        num1 = 0;
        num2 = 0;
    }//GEN-LAST:event_jBClearActionPerformed

    private void jBIgualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBIgualActionPerformed
        Toolkit.getDefaultToolkit().beep();
        try {
            if (num1 % 1 == 0 || num1 % 1 == 0) {
                num2 = Double.parseDouble(JTVisor.getText());
                switch (operacao) {
                    case '+':

                        resultado = num1 + num2;
                        break;
                    case '-':
                        resultado = num1 - num2;
                        break;
                    case '/':
                        if (num1 != 0) {
                            resultado = num1 / num2;
                            break;
                        } else {
                            JOptionPane.showMessageDialog(null, "Impossível Fazer Operação com Zero!!!", "Erro", JOptionPane.ERROR_MESSAGE);
                            break;
                        }
                    case '*':
                        resultado = num1 * num2;
                        break;


                }
                JTVisor.setText(String.valueOf(resultado));
            } else {
                JOptionPane.showMessageDialog(null, "Digite os números e a operação desejada para que se possa dar o resultado!!!", "Erro", JOptionPane.ERROR_MESSAGE);
            }
        } catch (NumberFormatException | HeadlessException a) {
            a.getMessage();
            JOptionPane.showMessageDialog(null, "Digite os números e a operação desejada para que se possa dar o resultado!!!", "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jBIgualActionPerformed

    private void jBCEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCEActionPerformed
        Toolkit.getDefaultToolkit().beep();
        num2 = 0;
        JTVisor.setText(String.valueOf(num1));
    }//GEN-LAST:event_jBCEActionPerformed

    private void jBBackspaceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBackspaceActionPerformed
        Toolkit.getDefaultToolkit().beep();
        temp = numeroAntes;
        JTVisor.setText(temp);
    }//GEN-LAST:event_jBBackspaceActionPerformed

    private void jBMenosMaisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBMenosMaisActionPerformed
        Toolkit.getDefaultToolkit().beep();
        try {
            if (temp.startsWith("-")) {
                temp = temp.replace("-", "+");
            } else if (temp.startsWith("+")) {
                temp = temp.replace("+", "-");
            } else {
                temp = "-" + temp;
            }
            JTVisor.setText(temp);
        } catch (Exception a) {
            a.toString();
            JOptionPane.showMessageDialog(null, "Digite algum número!!!", "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jBMenosMaisActionPerformed

    private void itemMenuSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemMenuSairActionPerformed
        System.exit(0);
    }//GEN-LAST:event_itemMenuSairActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        copia = JTVisor.getText();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        JTVisor.setText(copia);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenu3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu3MouseClicked

    }//GEN-LAST:event_jMenu3MouseClicked

    private void JTVisorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JTVisorActionPerformed
        JOptionPane.showMessageDialog(null, "Selecione os botões desejados!!!", "Erro", JOptionPane.ERROR_MESSAGE);
        JTVisor.setText("");
        jBClear.requestFocus();
    }//GEN-LAST:event_JTVisorActionPerformed
    AudioStream audio;
    private void jMenu5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu5MouseClicked
    }//GEN-LAST:event_jMenu5MouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (!"".equals(txtValor1.getText()) && !"".equals(txtValor2.getText()) && !"".equals(txtResultado.getText())) {
            JOptionPane.showMessageDialog(null, "O X da equação é igual a " + (Double.parseDouble(txtResultado.getText()) - (Double.parseDouble(txtValor2.getText())) / Double.parseDouble(txtValor1.getText())));
            txtNumeroRadiciacao.setText("");
            txtNumeroFatorial.setText("");
            txtBase.setText("");
            txtExpoente.setText("");
            txtNumero1.setText("");
            txtNumero2.setText("");
        } else if (!"".equals(txtNumeroRadiciacao.getText())) {
            JOptionPane.showMessageDialog(null, "A raiz quadrada de " + Integer.parseInt(txtNumeroRadiciacao.getText()) + " é: " + Math.sqrt(Integer.parseInt(txtNumeroRadiciacao.getText())));
            txtValor1.setText("");
            txtValor2.setText("");
            txtResultado.setText("");
            txtNumeroFatorial.setText("");
            txtBase.setText("");
            txtExpoente.setText("");
            txtNumero1.setText("");
            txtNumero2.setText("");
        } else if (!"".equals(txtNumeroFatorial.getText())) {
            long fatorial = 1;
            for (int i = Integer.parseInt(txtNumeroFatorial.getText()); i > 1; i--) {
                fatorial *= i;
            }
            JOptionPane.showMessageDialog(null, "O fatorial de " + txtNumeroFatorial.getText() + " é: " + fatorial);
            txtValor1.setText("");
            txtValor2.setText("");
            txtResultado.setText("");
            txtNumeroRadiciacao.setText("");
            txtBase.setText("");
            txtExpoente.setText("");
            txtNumero1.setText("");
            txtNumero2.setText("");
        } else if (!"".equals(txtBase.getText()) && !"".equals(txtExpoente.getText())) {
            long resultadoPotencia = 1;
            for (int i = 1; i <= Integer.parseInt(txtExpoente.getText()); i++) {
                resultadoPotencia *= Integer.parseInt(txtBase.getText());
            }
            JOptionPane.showMessageDialog(null, "O resultado de " + txtBase.getText() + " elevado a " + txtExpoente.getText() + " é: " + String.valueOf(resultadoPotencia));
            txtValor1.setText("");
            txtValor2.setText("");
            txtResultado.setText("");
            txtNumeroRadiciacao.setText("");
            txtNumeroFatorial.setText("");
            txtNumero1.setText("");
            txtNumero2.setText("");
        } else if (!txtNumero1.getText().equals("")) {
            try {
                if (radioNumeroBinario.isSelected()) {
                    if (Integer.parseInt(txtNumero1.getText()) <= 255) {
                        int n = Integer.parseInt(txtNumero1.getText());
                        String binario = "";
                        for (int i = 0; i < 8; i++) {
                            if (n >= vetN[i]) {
                                n -= vetN[i];
                                vetB[i] = 1;
                            } else {
                                vetB[i] = 0;
                            }
                        }
                        for (int i = 0; i < 8; i++) {
                            binario += String.valueOf(vetB[i]);
                        }
                        txtNumero2.setText(binario);
                    } else {
                        JOptionPane.showMessageDialog(null, "Digite números<=255");
                        txtNumero1.setText("");
                    }
                } else if (radioBinarioNumero.isSelected()) {
                    if (txtNumero1.getText().length() == 8) {
                        char[] binario = txtNumero1.getText().toCharArray();
                        int numero = 0;
                        for (int i = 0; i < 8; i++) {
                            if (binario[i] == '1') {
                                numero += vetN[i];
                            }
                        }
                        txtNumero2.setText(String.valueOf(numero));
                    } else {
                        JOptionPane.showMessageDialog(null, "Preencha os campos corretamente");

                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Preencha os campos");
                }
                txtNumero1.requestFocus();
            } catch (NumberFormatException | HeadlessException e) {
                JOptionPane.showMessageDialog(null, "Preencha os campos");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Digite algo em algum campo corretamente!", "Erro", JOptionPane.ERROR_MESSAGE);
        }
        txtValor1.setText("");
        txtValor2.setText("");
        txtResultado.setText("");
        txtNumeroRadiciacao.setText("");
        txtNumeroFatorial.setText("");
        txtBase.setText("");
        txtExpoente.setText("");
    }//GEN-LAST:event_jButton1ActionPerformed
    private void txtNumeroRadiciacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNumeroRadiciacaoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumeroRadiciacaoActionPerformed

    private void itemMenuFuncionalidadesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemMenuFuncionalidadesActionPerformed
        if ("Mais Funcionalidades".equals(itemMenuFuncionalidades.getText())) {
            JFCalc.this.setSize(830, 473);
            itemMenuFuncionalidades.setText("Menos Funcionalidades");
        } else {
            JFCalc.this.setSize(296, 450);
            itemMenuFuncionalidades.setText("Mais Funcionalidades");
        }
    }//GEN-LAST:event_itemMenuFuncionalidadesActionPerformed

    private void txtNumero1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumero1KeyReleased
        if (txtNumero1.getText().length() == 0) {
            txtNumero2.setText("");
        }
    }//GEN-LAST:event_txtNumero1KeyReleased

    private void radioNumeroBinarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioNumeroBinarioActionPerformed
        lblNumero1.setText("Número:");
        lblNumero2.setText("Binário:");
        txtNumero2.setEnabled(false);
        txtNumero1.setEnabled(true);
        txtNumero1.setText("");
        txtNumero2.setText("");
        txtNumero1.requestFocus();
    }//GEN-LAST:event_radioNumeroBinarioActionPerformed

    private void radioBinarioNumeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioBinarioNumeroActionPerformed
        lblNumero1.setText("Binário:");
        lblNumero2.setText("Número:");
        txtNumero2.setEnabled(false);
        txtNumero1.setEnabled(true);
        txtNumero1.setText("");
        txtNumero2.setText("");
        txtNumero1.requestFocus();
    }//GEN-LAST:event_radioBinarioNumeroActionPerformed

    private void itemMenuAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemMenuAbrirActionPerformed
jFileChooser1.setDialogTitle("Selecione um arquivo de música!");
        int i = jFileChooser1.showOpenDialog(this);
        if (i == JFileChooser.APPROVE_OPTION) {
            tocar(jFileChooser1.getSelectedFile());
        }
    }//GEN-LAST:event_itemMenuAbrirActionPerformed

    private void itemMenuFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemMenuFecharActionPerformed
        this.player.close();
        contMusicas--;
    }//GEN-LAST:event_itemMenuFecharActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        JFSobre jfs = new JFSobre();
        jfs.setVisible(true);
        JFCalc.this.setVisible(false);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                new JFCalc().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton JBSubtracao;
    private javax.swing.JTextField JTVisor;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JMenuItem itemMenuAbrir;
    private javax.swing.JMenuItem itemMenuFechar;
    private javax.swing.JMenuItem itemMenuFuncionalidades;
    private javax.swing.JMenuItem itemMenuSair;
    private javax.swing.JButton jB0;
    private javax.swing.JButton jB1;
    private javax.swing.JButton jB2;
    private javax.swing.JButton jB3;
    private javax.swing.JButton jB4;
    private javax.swing.JButton jB5;
    private javax.swing.JButton jB6;
    private javax.swing.JButton jB7;
    private javax.swing.JButton jB8;
    private javax.swing.JButton jB9;
    private javax.swing.JButton jBAdicao;
    private javax.swing.JButton jBBackspace;
    private javax.swing.JButton jBCE;
    private javax.swing.JButton jBClear;
    private javax.swing.JButton jBDivisao;
    private javax.swing.JButton jBIgual;
    private javax.swing.JButton jBMenosMais;
    private javax.swing.JButton jBMultiplicacao;
    private javax.swing.JButton jBPonto;
    private javax.swing.JButton jButton1;
    private javax.swing.JFileChooser jFileChooser1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JLabel lblBase;
    private javax.swing.JLabel lblExpoente;
    private javax.swing.JLabel lblIgualEquacao;
    private javax.swing.JLabel lblNumero1;
    private javax.swing.JLabel lblNumero2;
    private javax.swing.JLabel lblNumeroRadiciacao;
    private javax.swing.JLabel lblXEquacao;
    private javax.swing.JPanel pnBinario;
    private javax.swing.JPanel pnCalculadora;
    private javax.swing.JPanel pnEquacao1Grau;
    private javax.swing.JPanel pnFatorial;
    private javax.swing.JPanel pnPotenciacao;
    private javax.swing.JPanel pnRadiciacao;
    private javax.swing.JRadioButton radioBinarioNumero;
    private javax.swing.JRadioButton radioNumeroBinario;
    private javax.swing.JTextField txtBase;
    private javax.swing.JTextField txtExpoente;
    private javax.swing.JTextField txtNumero1;
    private javax.swing.JTextField txtNumero2;
    private javax.swing.JTextField txtNumeroFatorial;
    private javax.swing.JTextField txtNumeroRadiciacao;
    private javax.swing.JTextField txtResultado;
    private javax.swing.JTextField txtValor1;
    private javax.swing.JTextField txtValor2;
    // End of variables declaration//GEN-END:variables
}